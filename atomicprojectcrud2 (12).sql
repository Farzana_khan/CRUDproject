-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2016 at 07:45 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojectcrud2`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `deleted_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`) VALUES
(1, 'the thief of baghdad', NULL),
(5, 'structure', NULL),
(7, 'crime & punishment', NULL),
(14, 'GOT', NULL),
(15, 'Harry Potter 4', NULL),
(16, 'Book liar', NULL),
(17, 'The beast', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(121) NOT NULL,
  `deleted_at` varchar(121) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `deleted_at`) VALUES
(1, 'Chicago', NULL),
(7, 'NewYork', NULL),
(9, 'NewYork', NULL),
(10, 'Dallas', '1467053249'),
(11, 'Missisipi', NULL),
(12, 'Munich', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `address` varchar(212) NOT NULL,
  `deleted_at` varchar(212) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `address`, `deleted_at`) VALUES
(5, 'nazrana@gmail.com', NULL),
(7, 'messi@gmail.com', '1467026366'),
(8, 'namira@gmail.com', NULL),
(10, 'bitm@gmail.com', NULL),
(11, 'ayesha@gmail.com', '1467039441'),
(12, 'baby@gmail.com', '1467024831'),
(13, 'leo@yahoo.com', NULL),
(14, 'kaka@yahoo.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genderselect`
--

CREATE TABLE `genderselect` (
  `id` int(11) NOT NULL,
  `gender` varchar(212) NOT NULL,
  `deleted_at` varchar(121) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genderselect`
--

INSERT INTO `genderselect` (`id`, `gender`, `deleted_at`) VALUES
(1, 'male', '1467058255'),
(6, 'Male', '1467058119'),
(7, 'Female', '1467058079'),
(8, 'Female', '1467058257'),
(9, 'Male', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `hobbies` varchar(212) NOT NULL,
  `deleted_at` varchar(212) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobbies`, `deleted_at`) VALUES
(1, 'Swimming', '1466914405'),
(3, 'Travelling,Photography,Coding', NULL),
(8, 'Travelling,Photography', '1466919015'),
(11, 'Gardening,Photography,Coding', '1466918780'),
(12, 'Reading,Gardening', NULL),
(13, 'Gardening,Travelling,Coding', NULL),
(14, 'Reading', NULL),
(15, 'Coding,Browsing', NULL),
(16, 'Travelling,Photography', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(121) NOT NULL,
  `images` varchar(232) NOT NULL,
  `deleted_at` varchar(122) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(9, 'me3', '1466659690Flower3.jpg', NULL),
(10, 'flower8', '1467092660Flower2.jpg', NULL),
(14, 'flw2', '1467059456Flower1.jpg', '1467088224'),
(15, 'flw1', 'Flower1.jpg', '1467088232'),
(16, 'flw3', 'Flower3.jpg', NULL),
(17, 'flw2', 'Flower2.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summaryoforg`
--

CREATE TABLE `summaryoforg` (
  `id` int(121) NOT NULL,
  `summary` varchar(122) NOT NULL,
  `deleted_at` varchar(212) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summaryoforg`
--

INSERT INTO `summaryoforg` (`id`, `summary`, `deleted_at`) VALUES
(1, 'Its a software company.', NULL),
(2, '', NULL),
(3, '', NULL),
(4, '', NULL),
(5, 'Promoter of ATM banking', NULL),
(6, 'Abbreviation is Abul Khair Steel', '1467092368'),
(7, 'Abbreviation is Abul Khair Steel', '1467092371');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genderselect`
--
ALTER TABLE `genderselect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summaryoforg`
--
ALTER TABLE `summaryoforg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `genderselect`
--
ALTER TABLE `genderselect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `summaryoforg`
--
ALTER TABLE `summaryoforg`
  MODIFY `id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
