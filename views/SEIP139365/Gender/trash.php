<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Gender\Gender;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$gender=new Gender();
$gender->prepare($_GET);
$gender->trash();