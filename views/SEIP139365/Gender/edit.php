
<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Gender\Gender;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$gender=new Gender();
$gender->prepare($_GET);
$singleItem=$gender->view();
//Utility::d($singleItem);


?>



<!DOCTYPE html>
<html lang="en">

<head>
    <title>CRUD-GENDER</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Editing Gender of the user: </h1>

<div class="container">
    <form role="form" method="post" action="update.php">

        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singleItem['id']?>">
        </div>

        <div class="form-group">
            <br>

            <div class="radio">
                <label><input type="radio" name="gender" value="Male" <?php if($singleItem['gender']=="Male"){ ?>
                    checked <?php } ?>  >Male</label>

            </div>
            <div class="radio">
                <label><input type="radio" name="gender" value="Female" <?php if($singleItem['gender']=="Female"){ ?>
                        checked <?php } ?>  >Female</label>

            </div>


        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>




</html>