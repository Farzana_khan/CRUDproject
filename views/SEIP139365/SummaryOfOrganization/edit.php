
<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\SummaryOfOrganization\SummaryOfOrganization;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$summary=new SummaryOfOrganization();
$summary->prepare($_GET);
$singleItem=$summary->view();
//Utility::d($singleItem);


?>



<!DOCTYPE html>
<html lang="en">

<head>
    <title>CRUD-summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<h1>Editing summary of the user: </h1>

<div class="container">
    <form role="form" method="post" action="update.php">

        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singleItem['id']?>">
        </div>

        <div class="form-group">
            <br>

            <div class="form-group">

                <label for="comment">Summary of a organization:  </label>
                <textarea class="form-control" name="summary" rows="8" id="comment" <?php ?>></textarea>
            </div>


        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>




</html>