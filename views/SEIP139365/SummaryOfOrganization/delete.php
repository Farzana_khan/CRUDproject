<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\SummaryOfOrganization\SummaryOfOrganization;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$summary=new SummaryOfOrganization();
$summary->prepare($_GET);
$summary->delete();