<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-BOOK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <h2>ProfilePicture</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Enter Profile Name: </label>
            <input type="text" name="name" class="form-control"  placeholder="Enter Name of pic here " enctype="multipart/form-data">
        </div>
        <div class="form-group" >
            <label for="pwd">Upload Image here: </label>
            <br>
            <input type="file" name="image" class="form-control">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>