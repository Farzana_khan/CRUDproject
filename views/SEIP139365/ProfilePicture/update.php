<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP139365\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139365\Utility\Utility;
use App\Bitm\SEIP139365\Message\Message;




if((isset($_FILES['image'])&& !empty($_FILES['image']['name'])))
{
    $image_name = time() . $_FILES['image']['name'];
    $temporary_location = $_FILES['image']['tmp_name'];

   // unlink($_SERVER['DOCUMENT_ROOT'].'/Atomicproject_B22_Farzana_CRUD/Resources/Images/'.$single_info['images']);
   unlink($_SERVER['DOCUMENT_ROOT'].'/Atomicproject_B22_Farzana_CRUD/Resources/Images/'.$image_name);
    move_uploaded_file( $temporary_location,'../../../Resources/Images/'.$image_name);
    $_POST['image']=$image_name;
}

$profile_picture=new ImageUploader();
$profile_picture->prepare($_POST);
$profile_picture->update();
