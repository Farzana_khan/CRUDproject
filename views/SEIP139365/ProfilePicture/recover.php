<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139365\ProfilePicture\ImageUploader;
use App\Bitm\SEIP139365\Utility\Utility;
use App\Bitm\SEIP139365\Message\Message;

$profile_picture=new ImageUploader();
$profile_picture->prepare($_GET);
$profile_picture->recover();