<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP139365\ProfilePicture\ImageUploader;


$profile_picture= new ImageUploader();
$profile_picture->prepare($_GET);
$single_info=$profile_picture->view();

unlink($_SERVER['DOCUMENT_ROOT'].'/CRUDprac2/CRUDprac/Resources/Images/'.$single_info['images']);
$profile_picture->prepare($_GET);
$profile_picture->delete();