<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139365\Utility\Utility;

use App\Bitm\SEIP139365\Message\Message;
use App\Bitm\SEIP139365\ProfilePicture\ImageUploader;

$profile_picture = new ImageUploader ();
$profile_picture->prepare($_GET);
$single_info=$profile_picture->view();

//Utility::dd($single_info);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>



<body>

<div class="container">
    <h2>Edit Image</h2>
    <form role="form" method="post" action="update.php" enctype="multipart/form-data">

        <div class="form-group">
            <label>Edit Image Name:</label>
            <input type="hidden" name="id"  value="<?php echo $single_info['id']?>">
            <input type="text" name="name" class="form-control" value="<?php echo $single_info['name']?>">
        </div>

        <div class="form-group">
            <label for="pwd"> Upload new profile picture</label>
            <input type="file" name="image" class="form-control">
            <img src="../../../Resources/Images/ <?php echo $single_info['images']?>"alt="image" height="100" width="100" class="img-responsive">

        </div>



        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>