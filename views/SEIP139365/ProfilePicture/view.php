<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139365\Utility\Utility;
use App\Bitm\SEIP139365\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$profile_picture->prepare($_GET);
$singleItem = $profile_picture->view();
//Utility::d($singleItem);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Image View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<br><br>
<div class="container">
    <center><h2>View Image from the List</h2></center>
    <br><br>
    <ul class="list-group">
        <li class="list-group-item">ID:<strong><?php echo "   ".$singleItem['id']?></strong></li>
        <li class="list-group-item">Image:<strong><?php echo "   ".$singleItem['name']?></strong></li>
    </ul>
</div>

</body>
</html>

