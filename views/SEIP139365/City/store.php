<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\City\City;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$city=new City();
$city->prepare($_POST);
$city->store();