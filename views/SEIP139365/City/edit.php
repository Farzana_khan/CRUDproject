<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\City\City;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$city=new City();
$city->prepare($_GET);
$singleItem=$city->view();



?>


<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
</html>

<body>
<div class="container">
    <table class="table-responsive">
        <h1> Editing Cities: </h1>
        <form role="form" method="post" action="update.php">
            <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singleItem['id']?>">

                </div>
            <div class="form-group">
                <label for="sel1">Select list (select one):</label>
                <select class="form-control" name="cityname">
                    <option <?php if($singleItem['name']=="Rome"){?> selected="selected" <?php }?>>Rome</option>
                <option <?php if($singleItem['name']=="Milan"){?> selected="selected" <?php }?>>Milan</option>
                    <option <?php if($singleItem['name']=="Berlin"){?> selected="selected" <?php }?>>Berlin</option>

                <option <?php if($singleItem['name']=="Munich"){?> selected="selected" <?php }?>>Munich</option>

                <option <?php if($singleItem['name']=="NewYork"){?> selected="selected" <?php }?>>NewYork</option>
                <option <?php if($singleItem['name']=="Dallas"){?> selected="selected" <?php }?>>Dallas</option>

                <option <?php if($singleItem['name']=="California"){?> selected="selected" <?php }?>>California</option>
                <option <?php if($singleItem['name']=="Missisipi"){?> selected="selected" <?php }?>>Missisipi</option>
                </select>


                <input type="submit" value="Update">
            </div>

        </form>
    </table>
</div>
</body>
