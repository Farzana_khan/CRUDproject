<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP139365\Book\Utility;
use App\Bitm\SEIP139365\Book\Book;

$book = new Book();
$book->prepare($_GET);
$singleItem=$book->view();

//Utility::dd($singleBook);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>



<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem['id']?>">
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem['title']?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>