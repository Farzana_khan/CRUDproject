<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Birthday\Birthday;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$birthday=new Birthday();
$birthday->prepare($_POST);
$birthday->store();