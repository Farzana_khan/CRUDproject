<?php
include_once('../../../vendor/autoload.php');

use App\BITM\SEIP139365\Hobby\Hobby;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;



//Utility::d($_POST['hobby']);

$selected_hobby=$_POST['hobby'];  //array
//var_dump($selected_hobby);
$comma_separated=implode(",",$selected_hobby);  //string
//Utility::d($comma_separated);
$_POST['hobby']=$comma_separated;


$hobby=new Hobby();
$hobby->prepare($_POST);
$hobby->store();

