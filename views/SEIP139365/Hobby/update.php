<?php
include_once('../../../vendor/autoload.php');

use App\BITM\SEIP139365\Hobby\Hobby;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$_array=$_POST['hobby'];  //array
//Utility::d($_array);
$_hobbyString=implode(",",$_array);   //string
//Utility::d($_hobbyString);
$_POST['hobby']=$_hobbyString;

$hobby=new Hobby();
$hobby->prepare($_POST);
$hobby->update();
echo Message::message();
