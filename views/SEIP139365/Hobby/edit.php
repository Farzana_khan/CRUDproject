<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Hobby\Hobby;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$hobby=new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();

//Utility::d($singleItem);  //string
$_hobby=explode(",",$singleItem['hobbies']);    //array
//Utility::d($_hobby);


?>


<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
</html>

<body>
<div class="container">
    <table class="table-responsive">
        <h1> Editing Hobbies</h1>
        <form role="form" method="post" action="update.php">
            <input type="hidden" name="id" value="<?php echo $singleItem['id']?>">;
            <div class="checkbox">
                <label>  <input type="checkbox" name=hobby[] value="Reading" <?php if(in_array("Reading",$_hobby))
                {
                    echo "checked";
                }?>>Reading</label>

                <label><input type="checkbox" name=hobby[] value="Gardening" <?php if(in_array("Gardening",$_hobby))
                {
                    echo "checked";
                }
                ?> >Gardening</label>

                <label><input type="checkbox" name=hobby[] value="Travelling" <?php if(in_array("Travelling",$_hobby))
                {
                    echo "checked";
                }
                ?> >Travelling<label>

                    <label><input type="checkbox" name=hobby[] value="Photography" <?php if(in_array("Photography",$_hobby))
                {
                    echo "checked";
                }
                ?> >Photography</label>

                    <label><input type="checkbox" name=hobby[] value="Coding" <?php if(in_array("Coding",$_hobby))
                {
                    echo "checked";
                }
                ?> >Coding</label>

                    <label><input type="checkbox" name=hobby[] value="Browsing" <?php if(in_array("Browsing",$_hobby))
                {
                    echo "checked";
                }
                ?> >Browsing</label>



            </div>
            <input type="submit" value="Submit">
        </form>
    </table>
</div>
</body>
