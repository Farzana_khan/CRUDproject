<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Hobby\Hobby;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$hobby=new Hobby();

$allhobby=$hobby->index();



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<h1>Hobby List</h1>
<a href="create.php" role="button" class="btn btn-primary">Insert Again</a>
<a href="trashed_view.php" role="button" class="btn btn-primary">Trash List</a>
 <?php
 echo Message::message();
 ?>

<div class="table-responsive">
    <table class="table">
        <br> <br>
        <thead>
        <tr>
            <th>Serial</th>
            <th>Id</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <?php
            $sl=0;
            foreach($allhobby as $hobby)
            {
                $sl++;

            ?>

            <td><?php echo $sl?></td>
            <td><?php echo $hobby['id']?></td>
            <td> <?php echo $hobby['hobbies']?></td>


            <td> <a href="view.php?id= <?php echo $hobby['id'] ?>" role="button">View</a>

            <td> <a href="edit.php?id= <?php echo $hobby['id'] ?>" role="button">Edit</a>

            <td> <a href="delete.php?id= <?php echo $hobby['id'] ?>" role="button">Delete</a>

            <td> <a href="trash.php?id= <?php echo $hobby['id'] ?>" role="button">Trash</a>

            </td>



        </tr>
        <?php } ?>
        </tbody>


    </table>
</div>




</body>











</html>
