<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Email\Email;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$email=new Email();
$emailtrash=$email->trashed();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>CRUD-Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h1>Trash Table of Email:</h1>
    <br>
    <a href="index.php" class="btn btn-info" role="button">Back to Index List</a>
    <a href="trash_view.php" class="btn btn-primary" role="button">Trash List</a>
    <br>
    <form role="form" action="recoverMultiple.php" method="post">
        <div class="form-group" >

            <button type="submit" class="btn btn-primary">Recover all selected</button>

            <button type="button" class="btn btn-primary" id="delete">Delete all selected</button>



            <br>
    <div class="table-responsive">
        <table class="table">

            <tr>
                <thead>
                <th> Select</th>
                <th>Serial</th>
                <th>ID</th>
                <th>Email Address</th>
                <th>Action</th>
                </thead>
            </tr>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($emailtrash as $trash)
                {
                $sl++;

                ?>
                <td> <input type="checkbox" name=mark[] value="<?php echo $trash['id']?>"></td>
                <td> <?php echo $sl ?></td>
                <td> <?php echo $trash['id'] ?></td>
                <td> <?php echo $trash['address'] ?></td>

                <td>  <a href="recover.php?id=<?php echo $trash['id']?>" class="btn btn-info" role="button"> Recover</a>

                    <a href="delete.php?id=<?php echo $trash['id']?>" class="btn btn-info" role="button">Delete</a>

                </td>
            </tr>

            <?php } ?>
            </tbody>



        </table>
    </div>
        </div>
    </form>
</div>

<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
</script>
</body>





</html>



