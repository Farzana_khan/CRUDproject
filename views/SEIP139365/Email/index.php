<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Email\Email;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$email=new Email();
$allemail=$email->index();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>CRUD-BOOK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h1>Index Table of Email:</h1>
    <br>
    <a href="create.php" class="btn btn-info" role="button">Insert Again</a>
    <a href="trash_view.php" class="btn btn-primary" role="button">Trash List</a>
    <br>
    <?php
    echo Message::message()

    ?>
    <br>
    <div class="table-responsive">
        <table class="table">

        <tr>
            <thead>
                <th>Serial</th>
                <th>ID</th>
                <th>Email Address</th>
                <th>Action</th>
            </thead>
        </tr>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allemail as $email)
                {
                    $sl++;

                ?>

                <td> <?php echo $sl ?></td>
                <td> <?php echo $email['id'] ?></td>
                <td> <?php echo $email['address'] ?></td>

                <td>  <a href="view.php?id=<?php echo $email['id']?>" class="btn btn-info" role="button"> View</a>
                 <a href="edit.php?id=<?php echo $email['id']?>" class="btn btn-info" role="button">Edit </a>
                  <a href="delete.php?id=<?php echo $email['id']?>" class="btn btn-info" role="button">Delete</a>
                  <a href="trash.php?id=<?php echo $email['id']?>" class="btn btn-info" role="button">Trash</a>
                </td>
            </tr>

            <?php } ?>
            </tbody>



        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();
</script>



</body>
</html>



