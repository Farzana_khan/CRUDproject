<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP139365\Email\Email;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

$email=new Email();
$email->prepare($_GET);
$email->recover();