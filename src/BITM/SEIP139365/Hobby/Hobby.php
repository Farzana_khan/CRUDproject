<?php
namespace App\BITM\SEIP139365\Hobby;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

class Hobby
{
    public $id="";
    public $hobby="";
    public $conn;
    public $deleted_at;


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2") or die("Database connection failed");
    }


    public function prepare($data="")
    {
        if (array_key_exists("id",$data))
            $this->id=$data['id'];

        if(array_key_exists("hobby",$data))
            $this->hobby=$data['hobby'];
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectcrud2`.`hobby` (`hobbies`) VALUES ('".$this->hobby."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message
            ("Data has been inserted suuccessfully");
          //  Utility::redirect(index.php);
            header('Location:index.php');

        }
        else
        {
            echo "error";
        }
    }

    public function index()
    {
        $_allhobby=array();
        $query="SELECT * FROM `atomicprojectcrud2`.`hobby`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_allhobby[]=$row;

        }

        return $_allhobby;
    }

    public function view()
    {
        $query="SELECT * FROM `atomicprojectcrud2`.`hobby` WHERE id=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function delete()
    {
        $query="DELETE FROM  `atomicprojectcrud2`.`hobby` WHERE `hobby`.`id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
       if($result)
       {
          // Message::message("Deletion successful");
           echo "Deletion successful!";
           header('Location:index.php');
       }
        else
        {
            echo "Error!";
        }
    }

    public function update()
    {
        $query="UPDATE `atomicprojectcrud2`.`hobby` SET `hobbies` = '".$this->hobby."' WHERE `hobby`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
             Message::message("Update successful");
          //  echo "Update successful!";
            header('Location:index.php');
        }
        else
        {
            echo "Error!";
        }

    }

    public function trash()
    {
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectcrud2`.`hobby` SET `deleted_at` = '".$this->deleted_at."' WHERE `hobby`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Trash successful");
            //  echo "Update successful!";
            header('Location:index.php');
        }
        else
        {
           // Message::message("Trash not successful");
              echo "Trash not successful!";
           // header('Location:index.php');

        }
    }

    public function trashed()
    {
        $_trashed=array();
        $query="SELECT * FROM `atomicprojectcrud2`.`hobby` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_trashed[]=$row;
        }
        return $_trashed;

    }

    public function recover()
    {
        $query="UPDATE `atomicprojectcrud2`.`hobby` SET `deleted_at` = NULL  WHERE `hobby`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Recover successful");
            //  echo "Update successful!";
            header('Location:index.php');
        }
        else
        {
            // Message::message("Trash not successful");
            echo "Recover not successful!";
            // header('Location:index.php');

        }

    }

    public function recoverMultiple($idS=array())
    {
       // if(is_array($idS)) && count($idS)>0)
        //if((is_array($idS)) && count($idS)>0)
            if(is_array($idS) && count($idS>0))
        {
            $IDS=implode(",",$idS);
            $query="UPDATE `atomicprojectcrud2`.`hobby` SET `deleted_at` = NULL  WHERE `hobby`.`id` IN (".$IDS.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                Message::message("Recover successful");
                //  echo "Update successful!";
                header('Location:index.php');
            }
            else
            {
                // Message::message("Trash not successful");
                echo "Recover not successful!";
                // header('Location:index.php');

            }

        }


    }

    public function deleteMultiple($idS=array())
    {
        //if(is_array($idS) && count($idS)>0)
       // if((is_array($idS)) && count($idS)>0)
        if(is_array($idS) && count($idS>0))
        {
            $IDS=implode(",",$idS);
            $query="DELETE FROM`atomicprojectcrud2`.`hobby` WHERE `hobby`.`id` IN (".$IDS.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                Message::message("Recover successful");
                //  echo "Update successful!";
                header('Location:index.php');
            }
            else
            {
                // Message::message("Trash not successful");
                echo "Recover not successful!";
                // header('Location:index.php');

            }


        }









    }






}