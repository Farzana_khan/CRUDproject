<?php
namespace App\BITM\SEIP139365\Email;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;



class Email
{

    public $id;
    public $e_address;
    public $conn;
    public $deleted_at;


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2")or die("database connection failed");

    }

    public function prepare($data)
    {
        if (array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists("addressOfuser",$data))
        {
            $this->e_address=$data['addressOfuser'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectcrud2`.`email` ( `address`) VALUES ('".$this->e_address."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
             ("Insertion Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }

    }

    public function index()
    {
        $_allemail=array();
        $query="SELECT * FROM `atomicprojectcrud2`.`email`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_allemail[]=$row;
        }
        return $_allemail;

    }


    public function view()
    {

        $query="SELECT * FROM `atomicprojectcrud2`.`email` WHERE `email`.`id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;


    }

    public function delete()
    {
        $query="DELETE FROM `atomicprojectcrud2`.`email` WHERE `email`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
                ("Deletion Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }

    }

    public function update()
    {
        $query="UPDATE `atomicprojectcrud2`.`email` SET `address` = '".$this->e_address."' WHERE `email`.`id` = ".$this->id;

        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
                ("Update Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }
    }

    public function trash()
    {
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectcrud2`.`email` SET `deleted_at` = '".$this->deleted_at."' WHERE `email`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
                ("Trash Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }

    }

    public function trashed()
    {
        $_alltrashed=array();
        $query="SELECT * FROM `atomicprojectcrud2`.`email` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_alltrashed[]=$row;
        }
        return $_alltrashed;

    }

    public function recover()
    {
        $query="UPDATE `atomicprojectcrud2`.`email` SET `deleted_at` = NULL WHERE `email`.`id`= ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
                ("Recover Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }

    }

    public function recoverMultiple($idS=array())
    {
        if(is_array($idS) && count($idS>0))
        {
            $IDs=implode(",",$idS);
            $query="UPDATE `atomicprojectcrud2`.`email` SET `deleted_at`= NULL WHERE `id` IN (".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                Message::message("
                <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been recovered successfully.
        </div>");
               // Utility::redirect("index.php");
                header('Location:index.php');
            }
            else{
                echo "<br><br><br>Error!";

            }
        }



    }

    public function deleteMultiple($idS=array())
    {
        if(is_array($idS) && count($idS>0))
        {
            $IDs=implode(",",$idS);
            $query="DELETE FROM `atomicprojectcrud2`.`email` WHERE `id` IN (".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                Message::message("
                <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been deleted successfully.
        </div>");
               // Utility::redirect("index.php");
                header('Location:index.php');
            }
            else{
                echo "<br><br><br>Error!";
            }
        }



    }



}