<?php

namespace App\BITM\SEIP139365\ProfilePicture;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

class ImageUploader
{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;


    public function __construct($data="")
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2") or die("Database connection failed");

    }


    public function prepare($data=""){
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];    //create.php-> name="name"
        }

        if(array_key_exists("image",$data))
        {
            $this->image_name=$data['image'];  //create.php-> name="image" and image_njame is a variable
        }

        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        //echo  $this;

    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectcrud2`.`profilepicture` ( `name`, `images`) VALUES ('".$this->name."', '".$this->image_name."')";
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function index()
    {
        $_allinfo=array();
        $query="SELECT * FROM `atomicprojectcrud2`.`profilepicture` ";
        //echo $query;

        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_allinfo[]=$row;
        }
        return $_allinfo;

    }

    public function view()
    {
        $query="SELECT * FROM `atomicprojectcrud2`.`profilepicture` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {

        if(!empty($this->image_name)) {
            $query = "UPDATE `atomicprojectcrud2`.`profilepicture` SET `name`='".$this->name."', `images`='".$this->image_name."' WHERE `profilepicture`.`id` =" . $this->id;
        }
        else {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = '" . $this->name ."' WHERE `profilepicture`.`id` =" . $this->id;

        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete(){
        $query="DELETE FROM `atomicprojectcrud2`.`profilepicture`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }


    public function trash()
    {
       // $this->deleted_at=time();
        $query="UPDATE `atomicprojectcrud2`.`profilepicture` SET `deleted_at` = '".time()."' WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Book Title has been trashed.
            </div>");
            Utility::redirect('index.php');
        }
        else
        {
            echo "<br><br><br>Error!";
        }


    }

    public function trashed(){
        $_allinfo= array();
        $query="SELECT * FROM `atomicprojectcrud2`.`profilepicture` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);

        while($row= mysqli_fetch_assoc($result)){
            $_allinfo[]=$row;
        }

        return $_allinfo;

    }


    public function recover()
    {
        $query="UPDATE `atomicprojectcrud2`.`profilepicture` SET `deleted_at`= NULL WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been recovered successfully.
        </div>");
            Utility::redirect("index.php");
        }
        else{
            echo "<br><br><br>Error!";
        }


    }

    public function recoverMultiple($idS=array())
    {
        if(is_array($idS) && count($idS>0))
        {
            $IDs=implode(",",$idS);
            $query="UPDATE `atomicprojectcrud2`.`profilepicture` SET `deleted_at`= NULL WHERE `id` IN (".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                Message::message("
                <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been recovered successfully.
        </div>");
                Utility::redirect("index.php");
            }
            else{
                echo "<br><br><br>Error!";

            }
        }



    }

    public function deleteMultiple($idS=array())
    {
        if(is_array($idS) && count($idS>0))
        {
            $IDs=implode(",",$idS);
            $query="DELETE FROM `atomicprojectcrud2`.`profilepicture` WHERE `id` IN (".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if($result)
            {
                Message::message("
                <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been deleted successfully.
        </div>");
                Utility::redirect("index.php");
            }
            else{
                echo "<br><br><br>Error!";
            }
        }



    }















}