<?php
namespace App\BITM\SEIP139365\Gender;
use App\BITM\SEIP139365\Message\Message;
use App\BITM\SEIP139365\Utility\Utility;

class Gender
{
    public $id;
    public $gender;
    public $conn;
    public $deleted_at;


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectcrud2")or die("database connection failed");

    }

    public function prepare($data)
    {
        if (array_key_exists("gender",$data))
        {
            $this->gender=$data['gender'];
        }
        if (array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }

    }
public function store()
{
    $query="INSERT INTO `genderselect` ( `gender`) VALUES ( '".$this->gender."')";
    $result=mysqli_query($this->conn,$query);
    if($result)
    {
        Message::message
        ("Data has been inserted suuccessfully");
        //  Utility::redirect(index.php);
        header('Location:index.php');

    }
    else
    {
        echo "error";
    }
}
    public function index()
    {
        $_allcity = array();
        $query = "SELECT * FROM `atomicprojectcrud2`.`genderselect`";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result))
        {
            $_allcity[] = $row;
        }
        return $_allcity;
    }


    public function view()
    {
        $query="SELECT * FROM `atomicprojectcrud2`.`genderselect` WHERE `genderselect`.`id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }


    public function delete()
    {
        $query="DELETE FROM `atomicprojectcrud2`.`genderselect` WHERE `genderselect`.`id` =".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message
            ("Data has been deleted suuccessfully");
            //  Utility::redirect(index.php);
            header('Location:index.php');

        } else {
            echo "error";
        }

    }

    public function update()
    {
        $query="UPDATE `atomicprojectcrud2`.`genderselect` SET `gender` = '".$this->gender."' WHERE `genderselect`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Update successful");
            //  echo "Update successful!";
            header('Location:index.php');
        }
        else
        {
            echo "Error!";
        }

    }

    public function trash()
    {
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectcrud2`.`genderselect` SET `deleted_at` = '".$this->deleted_at."' WHERE `genderselect`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
                ("Trash Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }

    }

    public function trashed()
    {
        $_alltrashed=array();
        $query="SELECT * FROM `atomicprojectcrud2`.`genderselect` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $_alltrashed[]=$row;
        }
        return $_alltrashed;

    }

    public function recover()
    {
        $query="UPDATE `atomicprojectcrud2`.`genderselect` SET `deleted_at` = NULL WHERE `genderselect`.`id`= ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message(
                ("Recover Successful"));
            header('Location:index.php');
        }
        else
        {
            echo "Error!";

        }

    }

    public function recoverMultiple($idS=[])
    {
        if(is_array($idS) && count($idS)>0)
        {
            $IDs=implode(",",$idS);
            $query = "UPDATE `atomicprojectcrud2`.`genderselect` SET `deleted_at`= NULL WHERE `id` IN (".$IDs.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
        <div class=\"alert alert-success\">
        <strong>Recovered!</strong> Data has been recovered successfully.
        </div>");
                //Utility::redirect("index.php");
                header('Location:index.php');
            } else {
                echo "Error!!!";
            }
        }
    }

    public function deleteMultiple($idS=[])
    {
        if(is_array($idS) && count($idS)>0)
        {
            $IDs=implode(",",$idS);
            $query = "DELETE FROM `atomicprojectcrud2`.`genderselect` WHERE `id` IN (".$IDs.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
        <div class=\"alert alert-success\">
        <strong>Deleted!</strong> Data has been deleted successfully.
        </div>");
                //Utility::redirect("index.php");
                header('Location:index.php');

            } else {
                echo "Error!!!";
            }
        }
    }












}